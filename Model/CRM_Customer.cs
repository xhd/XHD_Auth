﻿/*
* CRM_Customer.cs
*
* 功 能： N/A
* 类 名： CRM_Customer
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2016-02-29 11:53:22    黄润伟    
*
* Copyright © 2015 www.xhdcrm.com All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
	/// <summary>
	/// CRM_Customer:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CRM_Customer
	{
		public CRM_Customer()
		{}
		#region Model
		private string _id;		
		private string _cus_name;
        private string _cus_tel;		
		private string _remarks;
		private string _emp_id;
		private string _create_id;
		private DateTime? _create_time;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string cus_name
		{
			set{ _cus_name=value;}
			get{return _cus_name;}
		}
		

        public string cus_tel
        {
            set { _cus_tel = value; }
            get { return _cus_tel; }
        }

		/// <summary>
		/// 
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string emp_id
		{
			set{ _emp_id=value;}
			get{return _emp_id;}
		}
				
		/// <summary>
		/// 
		/// </summary>
		public string create_id
		{
			set{ _create_id=value;}
			get{return _create_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? create_time
		{
			set{ _create_time=value;}
			get{return _create_time;}
		}
		#endregion Model

	}
}

