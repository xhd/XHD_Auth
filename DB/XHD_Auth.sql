USE [XHD_Auth]
GO
/****** Object:  Table [dbo].[Sys_role_emp]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_role_emp](
	[RoleID] [varchar](50) NOT NULL,
	[empID] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_role_emp] ([RoleID], [empID]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045')
INSERT [dbo].[Sys_role_emp] ([RoleID], [empID]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'CD1977ED-2323-491A-BA06-8593878C67C9')
/****** Object:  Table [dbo].[Sys_role]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_role](
	[id] [varchar](50) NOT NULL,
	[RoleName] [nvarchar](255) NULL,
	[RoleDscript] [nvarchar](255) NULL,
	[RoleSort] [int] NULL,
	[DataAuth] [int] NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_Sys_role] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_role] ([id], [RoleName], [RoleDscript], [RoleSort], [DataAuth], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'系统管理员', N'', 0, 5, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C868B7 AS DateTime))
/****** Object:  Table [dbo].[Sys_online]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_online](
	[UserID] [varchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[LastLogTime] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_online] ([UserID], [UserName], [LastLogTime]) VALUES (N'38295F35-8507-4254-B727-B8FF26E9E302', N'超级管理员', CAST(0x0000A74C00F4C4D1 AS DateTime))
/****** Object:  Table [dbo].[Sys_Menu]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Menu](
	[Menu_id] [varchar](50) NOT NULL,
	[Menu_name] [varchar](255) NULL,
	[parentid] [varchar](50) NULL,
	[App_id] [varchar](50) NULL,
	[Menu_url] [varchar](255) NULL,
	[Menu_icon] [varchar](50) NULL,
	[Menu_order] [int] NULL,
	[Menu_type] [varchar](50) NULL,
 CONSTRAINT [PK_Sys_Menu] PRIMARY KEY NONCLUSTERED 
(
	[Menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'base_button', N'按钮管理', N'root', N'App_sys', N'system/sysbase/Sys_Button.aspx', N'images/icon/85.png', 999, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'base_menu', N'目录管理', N'root', N'App_sys', N'system/sysbase/Sys_Menu.aspx', N'images/icon/81.png', 888, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'crm_customer', N'客户管理', N'root', N'App_CRM', N'crm/customer.aspx', N'images/icon/37.png', 10, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'Sys_role', N'角色授权', N'root', N'App_sys', N'System/sysmanager/Sys_role.aspx', N'images/icon/70.png', 30, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'hr', N'用户管理', N'root', N'App_sys', N'', N'images/icon/37.png', 10, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'hr_department', N'组织架构', N'hr', N'App_sys', N'hr/hr_department.aspx', N'images/icon/67.png', 20, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'hr_employee', N'员工管理', N'hr', N'App_sys', N'hr/hr_employee.aspx', N'images/icon/37.png', 40, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'hr_position', N'职务管理', N'hr', N'App_sys', N'hr/hr_position.aspx', N'images/icon/68.png', 20, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'hr_post', N'岗位管理', N'hr', N'App_sys', N'hr/hr_post.aspx', N'images/icon/49.png', 30, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'Sys_log_err', N'错误日志', N'root', N'App_sys', N'system/sysbase/sys_log_err.aspx', N'images/icon/51.png', 50, N'sys')
INSERT [dbo].[Sys_Menu] ([Menu_id], [Menu_name], [parentid], [App_id], [Menu_url], [Menu_icon], [Menu_order], [Menu_type]) VALUES (N'Sys_log', N'日志管理', N'root', N'App_sys', N'system/sysmanager/Sys_log.aspx', N'images/icon/51.png', 40, N'sys')
/****** Object:  Table [dbo].[Sys_log_Err]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_log_Err](
	[id] [varchar](50) NOT NULL,
	[Err_typeid] [int] NULL,
	[Err_type] [nvarchar](250) NULL,
	[Err_time] [datetime] NULL,
	[Err_url] [varchar](500) NULL,
	[Err_message] [nvarchar](max) NULL,
	[Err_source] [varchar](500) NULL,
	[Err_trace] [nvarchar](max) NULL,
	[Err_emp_id] [varchar](50) NULL,
	[Err_emp_name] [nvarchar](250) NULL,
	[Err_ip] [varchar](250) NULL,
 CONSTRAINT [PK_Sys_log_Err] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_log]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_log](
	[id] [varchar](50) NOT NULL,
	[EventType] [nvarchar](250) NULL,
	[EventID] [varchar](50) NULL,
	[EventTitle] [nvarchar](250) NULL,
	[Log_Content] [nvarchar](max) NULL,
	[UserID] [varchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[IPStreet] [varchar](50) NULL,
	[EventDate] [datetime] NULL,
 CONSTRAINT [PK_Sys_log] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'BF193536-252D-4AE3-B723-EA235D275F4E', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00BE099C AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'E487A860-C2F3-429B-959C-334E84FFD62A', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00BE8218 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'08d3b503-2bbb-473e-8b4a-adb2d5e12d53', N'权限调整', N'5220B72B-53C1-490E-8BE9-78A93D1A467C', NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00C9D226 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'a01c6a80-5c86-4863-aecf-587fd10e63f0', N'权限人员调整', N'5220B72B-53C1-490E-8BE9-78A93D1A467C', NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00ED7536 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'1FB528DB-4C6B-4DB9-A69A-006F566C8866', N'角色修改', N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'系统管理员', N'【数据权限】全部 → 本人', N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00ED7FFB AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'5AE85951-C332-4044-8843-1F7A97E50F51', N'系统登录', NULL, NULL, NULL, N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', NULL, N'::1', CAST(0x0000A74C00ED910D AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'73f614b6-3ae9-4e4f-86e8-9d61ef86e321', N'权限调整', N'5220B72B-53C1-490E-8BE9-78A93D1A467C', NULL, NULL, N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', NULL, N'::1', CAST(0x0000A74C00ED9E40 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'A22D9B99-84C1-42F4-9CF6-85C17D257946', N'系统登录', NULL, NULL, NULL, N'CD1977ED-2323-491A-BA06-8593878C67C9', NULL, N'::1', CAST(0x0000A74C00EDCD13 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'D0877DCF-73B2-4543-B7EF-E5CFEF0F6659', N'角色修改', N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'系统管理员', N'【数据权限】本人 → 全部', N'CD1977ED-2323-491A-BA06-8593878C67C9', NULL, N'::1', CAST(0x0000A74C00EDE2F7 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'FB7DE967-3D07-42BF-9F9D-E4E2A1FEFD1F', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00F132F1 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'8585D8F2-7B27-463C-9D11-5E064B2B40C4', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00F2CAD2 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'012E3D29-FFBB-4F84-B6FA-AE91DBD3C578', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00F3F0E6 AS DateTime))
INSERT [dbo].[Sys_log] ([id], [EventType], [EventID], [EventTitle], [Log_Content], [UserID], [UserName], [IPStreet], [EventDate]) VALUES (N'30EE6232-F027-428C-B187-DB827485D594', N'系统登录', NULL, NULL, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', NULL, N'::1', CAST(0x0000A74C00F47E53 AS DateTime))
/****** Object:  Table [dbo].[Sys_data_authority]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_data_authority](
	[Role_id] [varchar](50) NULL,
	[dep_id] [varchar](50) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Button]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Button](
	[Btn_id] [varchar](50) NOT NULL,
	[Btn_name] [nvarchar](255) NULL,
	[Btn_type] [varchar](50) NULL,
	[Btn_icon] [varchar](50) NULL,
	[Btn_handler] [varchar](255) NULL,
	[Menu_id] [varchar](50) NULL,
	[Menu_name] [nvarchar](255) NULL,
	[Btn_order] [int] NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_Sys_Button] PRIMARY KEY NONCLUSTERED 
(
	[Btn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'89C9BE0D-F21C-4F63-9CD4-6E565EB10EA0', N'新增', NULL, N'images/icon/11.png', N'add()', N'hr_employee', N'员工管理', 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'1D4CA3FD-7297-43FE-86AF-C3C101926508', N'删除', NULL, N'images/icon/12.png', N'del()', N'hr_employee', N'员工管理', 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'361DEB84-BE21-41DF-8579-1A7D23F9BDBB', N'修改', NULL, N'images/icon/33.png', N'edit()', N'hr_employee', N'员工管理', 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'1B2D5C53-D948-485C-9CD7-448CD46975B3', N'新增', NULL, N'images/icon/11.png', N'add()', N'Sys_role', N'角色授权', 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'2EB328DD-E7F3-426D-BCD3-CA47FB6AAB30', N'删除', NULL, N'images/icon/12.png', N'del()', N'Sys_role', N'角色授权', 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'CB896640-9FAE-4754-8574-48FF74604AF0', N'修改', NULL, N'images/icon/33.png', N'edit()', N'Sys_role', N'角色授权', 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'44B75D22-852E-4E09-A7CC-D4E71C90E86B', N'操作权限', NULL, N'images/icon/91.png', N'authorized()', N'Sys_role', N'角色授权', 40, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'4FC40BF8-B12B-4BF0-8F31-897031A10312', N'包含人员', NULL, N'images/icon/37.png', N'role_emp()', N'Sys_role', N'角色授权', 60, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'BFA968C8-AEA7-40A1-8192-541272932B6E', N'新增', NULL, N'images/icon/11.png', N'add()', N'hr_department', NULL, 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'4E0A9A25-5608-440C-8D14-5261F6CA436D', N'删除', NULL, N'images/icon/12.png', N'del()', N'hr_department', NULL, 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'A70CCF6B-CA76-474D-BF93-C9C2D33324BD', N'修改', NULL, N'images/icon/33.png', N'edit()', N'hr_department', NULL, 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'6A513DCC-52F4-4150-B78D-01F84CEBD671', N'新增', NULL, N'images/icon/11.png', N'add()', N'hr_position', NULL, 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'9E686C8B-A2CC-4456-988B-88205728E487', N'删除', NULL, N'images/icon/12.png', N'del()', N'hr_position', NULL, 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'BB9C94AC-A3FD-4409-8468-7965BDEC0134', N'新增', NULL, N'images/icon/11.png', N'add()', N'hr_post', NULL, 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'B34D6236-66F9-4997-90F3-4453C0832CB6', N'修改', NULL, N'images/icon/33.png', N'edit()', N'hr_position', NULL, 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'3754BE8F-5D53-47AD-9643-3B827F972A78', N'修改', NULL, N'images/icon/33.png', N'edit()', N'hr_post', NULL, 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'02674D56-664F-4F76-98EA-E15D14F45612', N'删除', NULL, N'images/icon/12.png', N'del()', N'hr_post', NULL, 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'420BB111-95AA-42D1-B686-2EC544033D27', N'修改密码', NULL, N'images/icon/77.png', N'changepwd()', N'hr_employee', NULL, 40, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'2C98AF57-72FB-4467-82C9-3D400D48B04C', N'新增', NULL, N'images/icon/11.png', N'add()', N'crm_customer', NULL, 10, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'4E97A812-0E5F-476D-8D67-8F71A02504FA', N'修改', NULL, N'images/icon/33.png', N'edit()', N'crm_customer', NULL, 20, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'1F71C5EF-6541-43D3-9023-5E8470EB88AF', N'删除', NULL, N'images/icon/12.png', N'del()', N'crm_customer', NULL, 30, NULL, NULL)
INSERT [dbo].[Sys_Button] ([Btn_id], [Btn_name], [Btn_type], [Btn_icon], [Btn_handler], [Menu_id], [Menu_name], [Btn_order], [create_id], [create_time]) VALUES (N'238F63DD-2CC8-4385-83BC-E2E3B09B15C3', N'跨部权限', NULL, N'images/icon/85.png', N'data_authorized()', N'Sys_role', NULL, 50, NULL, NULL)
/****** Object:  Table [dbo].[Sys_authority]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_authority](
	[Role_id] [varchar](50) NOT NULL,
	[App_id] [varchar](50) NULL,
	[Auth_type] [int] NULL,
	[Auth_id] [varchar](50) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'hr', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'hr_department', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'hr_position', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'hr_post', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'hr_employee', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'Sys_role', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 0, N'Sys_log', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'ligerui1000', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'ligerui1001', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'BFA968C8-AEA7-40A1-8192-541272932B6E', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'A70CCF6B-CA76-474D-BF93-C9C2D33324BD', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'4E0A9A25-5608-440C-8D14-5261F6CA436D', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'6A513DCC-52F4-4150-B78D-01F84CEBD671', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'B34D6236-66F9-4997-90F3-4453C0832CB6', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'9E686C8B-A2CC-4456-988B-88205728E487', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'BB9C94AC-A3FD-4409-8468-7965BDEC0134', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'3754BE8F-5D53-47AD-9643-3B827F972A78', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'02674D56-664F-4F76-98EA-E15D14F45612', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'89C9BE0D-F21C-4F63-9CD4-6E565EB10EA0', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'361DEB84-BE21-41DF-8579-1A7D23F9BDBB', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'1D4CA3FD-7297-43FE-86AF-C3C101926508', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'420BB111-95AA-42D1-B686-2EC544033D27', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'1B2D5C53-D948-485C-9CD7-448CD46975B3', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'CB896640-9FAE-4754-8574-48FF74604AF0', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'2EB328DD-E7F3-426D-BCD3-CA47FB6AAB30', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'44B75D22-852E-4E09-A7CC-D4E71C90E86B', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'238F63DD-2CC8-4385-83BC-E2E3B09B15C3', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_sys', 1, N'4FC40BF8-B12B-4BF0-8F31-897031A10312', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00C9D222 AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 0, N'crm_customer', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 1, N'ligerui1000', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 1, N'ligerui1001', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 1, N'2C98AF57-72FB-4467-82C9-3D400D48B04C', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 1, N'4E97A812-0E5F-476D-8D67-8F71A02504FA', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
INSERT [dbo].[Sys_authority] ([Role_id], [App_id], [Auth_type], [Auth_id], [create_id], [create_time]) VALUES (N'5220B72B-53C1-490E-8BE9-78A93D1A467C', N'App_CRM', 1, N'1F71C5EF-6541-43D3-9023-5E8470EB88AF', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00ED9E3F AS DateTime))
/****** Object:  Table [dbo].[Sys_App]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_App](
	[id] [varchar](50) NOT NULL,
	[App_name] [nvarchar](100) NULL,
	[App_order] [int] NULL,
	[App_url] [varchar](250) NULL,
	[App_handler] [varchar](250) NULL,
	[App_type] [varchar](50) NULL,
	[App_icon] [varchar](250) NULL,
 CONSTRAINT [PK_Sys_App] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Sys_App] ([id], [App_name], [App_order], [App_url], [App_handler], [App_type], [App_icon]) VALUES (N'App_CRM', N'客户管理', 20, NULL, N'fa-phone-square', NULL, N'images/icons/64X64/document.png')
INSERT [dbo].[Sys_App] ([id], [App_name], [App_order], [App_url], [App_handler], [App_type], [App_icon]) VALUES (N'App_sys', N'系统管理', 80, NULL, N'fa-desktop', NULL, N'images/icons/64X64/gears.png')
/****** Object:  Table [dbo].[hr_post]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hr_post](
	[id] [varchar](50) NOT NULL,
	[post_name] [nvarchar](255) NULL,
	[position_id] [varchar](50) NULL,
	[dep_id] [varchar](50) NULL,
	[emp_id] [varchar](50) NULL,
	[default_post] [int] NULL,
	[note] [nvarchar](max) NULL,
	[post_descript] [nvarchar](max) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_hr_post] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[hr_post] ([id], [post_name], [position_id], [dep_id], [emp_id], [default_post], [note], [post_descript], [create_id], [create_time]) VALUES (N'0A68F33A-CBB0-4E8D-A45D-83BBC70E8C1F', N'总经理', N'1EE864A4-453E-43AC-B517-5EC41EDA775C', N'4B5AD7E9-6A03-44EF-B341-28D61B810A10', N'CD1977ED-2323-491A-BA06-8593878C67C9', 1, N'', NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00CB7281 AS DateTime))
INSERT [dbo].[hr_post] ([id], [post_name], [position_id], [dep_id], [emp_id], [default_post], [note], [post_descript], [create_id], [create_time]) VALUES (N'CB5C5CBF-04F4-475B-A412-C52D986CA444', N'副总经理', N'1EE864A4-453E-43AC-B517-5EC41EDA775C', N'4B5AD7E9-6A03-44EF-B341-28D61B810A10', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', 1, N'', NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00ED3E18 AS DateTime))
/****** Object:  Table [dbo].[hr_position]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hr_position](
	[id] [varchar](50) NOT NULL,
	[position_name] [nvarchar](250) NULL,
	[position_order] [int] NULL,
	[position_level] [varchar](50) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_hr_position] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[hr_position] ([id], [position_name], [position_order], [position_level], [create_id], [create_time]) VALUES (N'1EE864A4-453E-43AC-B517-5EC41EDA775C', N'总经理', 20, N'20', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00CB3171 AS DateTime))
/****** Object:  Table [dbo].[hr_employee]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hr_employee](
	[id] [varchar](50) NOT NULL,
	[uid] [varchar](50) NULL,
	[pwd] [varchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[idcard] [varchar](50) NULL,
	[birthday] [varchar](50) NULL,
	[dep_id] [varchar](50) NULL,
	[post_id] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[sex] [nvarchar](50) NULL,
	[tel] [varchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[position_id] [varchar](50) NULL,
	[sort] [int] NULL,
	[EntryDate] [varchar](50) NULL,
	[address] [nvarchar](255) NULL,
	[remarks] [nvarchar](255) NULL,
	[education] [nvarchar](50) NULL,
	[level] [varchar](50) NULL,
	[professional] [nvarchar](50) NULL,
	[schools] [nvarchar](255) NULL,
	[title] [nvarchar](255) NULL,
	[portal] [varchar](250) NULL,
	[theme] [varchar](250) NULL,
	[canlogin] [int] NULL,
	[default_city] [varchar](50) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_hr_employee] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[hr_employee] ([id], [uid], [pwd], [name], [idcard], [birthday], [dep_id], [post_id], [email], [sex], [tel], [status], [position_id], [sort], [EntryDate], [address], [remarks], [education], [level], [professional], [schools], [title], [portal], [theme], [canlogin], [default_city], [create_id], [create_time]) VALUES (N'38295F35-8507-4254-B727-B8FF26E9E302', N'admin', N'E10ADC3949BA59ABBE56E057F20F883E', N'超级管理员', N'', N'', N'0', N'0', N'', N'男', N'', NULL, N'0', NULL, NULL, N'', NULL, N'', NULL, N'', N'', N'file/737836317958900229420120318/header/2017031423073695480.png', NULL, NULL, 1, NULL, NULL, NULL)
INSERT [dbo].[hr_employee] ([id], [uid], [pwd], [name], [idcard], [birthday], [dep_id], [post_id], [email], [sex], [tel], [status], [position_id], [sort], [EntryDate], [address], [remarks], [education], [level], [professional], [schools], [title], [portal], [theme], [canlogin], [default_city], [create_id], [create_time]) VALUES (N'CD1977ED-2323-491A-BA06-8593878C67C9', N'hrw', N'E10ADC3949BA59ABBE56E057F20F883E', N'黄润伟', N'', N'', N'4B5AD7E9-6A03-44EF-B341-28D61B810A10', N'0A68F33A-CBB0-4E8D-A45D-83BBC70E8C1F', N'', N'男', N'', N'在职', N'1EE864A4-453E-43AC-B517-5EC41EDA775C', NULL, N'', N'', N'', N'', NULL, N'', N'', N'', NULL, NULL, 1, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00CB6BB5 AS DateTime))
INSERT [dbo].[hr_employee] ([id], [uid], [pwd], [name], [idcard], [birthday], [dep_id], [post_id], [email], [sex], [tel], [status], [position_id], [sort], [EntryDate], [address], [remarks], [education], [level], [professional], [schools], [title], [portal], [theme], [canlogin], [default_city], [create_id], [create_time]) VALUES (N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', N'test', N'E10ADC3949BA59ABBE56E057F20F883E', N'测试', N'', N'', N'4b5ad7e9-6a03-44ef-b341-28d61b810a10', N'cb5c5cbf-04f4-475b-a412-c52d986ca444', N'', N'女', N'13467644655', N'在职', N'1ee864a4-453e-43ac-b517-5ec41eda775c', NULL, N'', N'', N'', N'', NULL, N'', N'', N'', NULL, NULL, 1, NULL, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00ED65F7 AS DateTime))
/****** Object:  Table [dbo].[hr_department]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hr_department](
	[id] [varchar](50) NOT NULL,
	[dep_name] [nvarchar](50) NULL,
	[parentid] [varchar](50) NULL,
	[parentname] [nvarchar](50) NULL,
	[dep_type] [nvarchar](50) NULL,
	[dep_icon] [varchar](50) NULL,
	[dep_chief] [nvarchar](50) NULL,
	[dep_tel] [varchar](50) NULL,
	[dep_fax] [varchar](50) NULL,
	[dep_add] [nvarchar](255) NULL,
	[dep_email] [varchar](50) NULL,
	[dep_descript] [nvarchar](255) NULL,
	[dep_order] [int] NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_hr_department] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[hr_department] ([id], [dep_name], [parentid], [parentname], [dep_type], [dep_icon], [dep_chief], [dep_tel], [dep_fax], [dep_add], [dep_email], [dep_descript], [dep_order], [create_id], [create_time]) VALUES (N'AC2E8C3B-4359-403F-B85D-2DF59678BF29', N'小黄豆软件', N'root', NULL, N'公司', N'images/icon/61.png', N'', N'', N'', N'', N'', N'', 20, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00CB0E1E AS DateTime))
INSERT [dbo].[hr_department] ([id], [dep_name], [parentid], [parentname], [dep_type], [dep_icon], [dep_chief], [dep_tel], [dep_fax], [dep_add], [dep_email], [dep_descript], [dep_order], [create_id], [create_time]) VALUES (N'4B5AD7E9-6A03-44EF-B341-28D61B810A10', N'总经办', N'AC2E8C3B-4359-403F-B85D-2DF59678BF29', NULL, N'部门', N'images/icon/88.png', N'', N'', N'', N'', N'', N'', 20, N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00CB2235 AS DateTime))
/****** Object:  Table [dbo].[CRM_Customer]    Script Date: 04/05/2017 14:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CRM_Customer](
	[id] [varchar](50) NOT NULL,
	[cus_name] [varchar](250) NULL,
	[cus_tel] [varchar](250) NULL,
	[Remarks] [varchar](4000) NULL,
	[emp_id] [varchar](50) NULL,
	[create_id] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_CRM_Customer] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[CRM_Customer] ([id], [cus_name], [cus_tel], [Remarks], [emp_id], [create_id], [create_time]) VALUES (N'0595685c-e16b-4c31-886d-e11fbf2a382a', N'黄润伟', N'13467644655', N'', N'CD1977ED-2323-491A-BA06-8593878C67C9', N'38295F35-8507-4254-B727-B8FF26E9E302', CAST(0x0000A74C00E58E87 AS DateTime))
INSERT [dbo].[CRM_Customer] ([id], [cus_name], [cus_tel], [Remarks], [emp_id], [create_id], [create_time]) VALUES (N'afb00c31-2ed1-41a5-aa01-749bc6341f88', N'小黄豆', N'13467644655', N'', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', N'AC9471A5-0020-4EAE-8FDB-E3C61EE17045', CAST(0x0000A74C00EDBA8A AS DateTime))
