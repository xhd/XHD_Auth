<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>小黄豆权限系统</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <%--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>--%>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico" />
    <link href="lib/ligerUI/skins/touch/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" />

    <script src="lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="JS/jquery.jclock.js" type="text/javascript"></script>
    <script src="JS/XHD.js" type="text/javascript"></script>
</head>
<body>

    <div id="pageloading"></div>
    <div id="header">


        <div class="logoContent">
            <span id="company">小黄豆CRM专业版</span>
        </div>
        <div class="navright">


            <div id="userinfo" class="item">

                <div style="position: relative; float: left;">
                    <div id="Username" style="font-size: 14px; padding-right: 5px; width: 60px; text-align: right; float: left; height: 50px; line-height: 50px;"></div>
                </div>
                <div id="portrait">
                    <img id="userheader" width="30" style="border-radius: 100%; margin-top: -20px;" />
                </div>
            </div>


        </div>
    </div>
    <div id="layout" style="width: 100%; margin-top: 4px;">

        <div position="left" title="功能菜单" id="accordion1">
            <%--<ul class="nav" id="mainmenu">
              
            </ul>--%>
        </div>
        <div position="center" id="framecenter">
            <div tabid="home" title="桌面" style="padding: 10px; background: #fff;">
                <p>
                    <span style="font-size: 30px; font-weight: bolder;">欢迎使用，小黄豆权限系统</span>
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;小黄豆权限系统，是从小黄豆CRM中分离出来的一套专业权限管理系统。
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;小黄豆权限系统，具有一下特点：
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;1、前后端完全分离。前端基于Jquery Ligerui 1.1.1开发，后台使用C#开发，通过Ajax技术传送和接收数据。实际上，因为前后端完全分离，后台完全可以是任何其他技术，比如java等。
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;2、细粒度的权限设计。权限系统的权限精细的控制到目录和每一个按钮，操作方便的同时，也能够灵活的设置权限。
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;3、数据权限设置。数据权限包含【本人】【本部】【本部及下级】【跨部】【全部】这几个维度，可以全方位的控制数据的显示范围，对不同的角色的可操作范围定义。
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;4、基于角色设计。小黄豆权限基于角色设计，使用人员可以定义多个角色，获取的权限采用交集，从而简化权限操作。<br />
                </p>
                <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;小黄豆CRM自2014年开源以来，受到了广大企业和广大开发者的喜爱，应广大开发者的要求，现在特将权限系统单独出来，基于 <a href="http://creativecommons.org/licenses/MIT/" target="_blank">MIT</a> 协议发布。<br />
                </p>
                <br />
                <p>小黄豆CRM官网：http://www.xhdcrm.com</p>
                <p>小黄豆CRM论坛：http://www.xhdcrm.com</p>
                <p>小黄豆CRM作者：黄润伟</p>
                <p>QQ：250476029</p>

            </div>
        </div>

        <div position="bottom">

            <div class="foot-right"></div>

        </div>
    </div>


    <script type="text/javascript">
        var tab;
        var accordion;
        function onResize() {
            var winH = $(window).height(), winW = $(window).width();
            $("#pageloading").height(winH);
            initLayout();
        }

        function checkbrowse() {
            if ($.browser.msie) {
                var ver = $.browser.version;

                if (ver == "6.0" || ver == "7.0" || ver == "8.0" || ver == "9.0") {
                    $.ligerDialog.warn("检测到您的浏览器版本较低，为了使系统得到最佳体验效果，建议您使用高级浏览器。")
                }
            }
        }
        $(function () {
            //$("#home").attr("src", "home/home.aspx");
            f_user();
            //布局初始化 
            var bodyHeight = $(window).height() - 65;
            //Tab
            tab = $("#framecenter").ligerTab({
                height: bodyHeight,
                dblClickToClose: true,
                showSwitch: true,       //显示切换窗口按钮
                showSwitchInTab: true //切换窗口按钮显示在最后一项 
            });


            getuserinfo();
            //remind();

            $("#userinfo").click(function (e) { f_dropdown(e) });
            onResize();
            $(window).resize(function () {
                onResize();
            });

            setInterval("getUser()", 30000);
            checkbrowse();

            //布局
            $("#layout").ligerLayout({ leftWidth: 190, rightWidth: 190, bottomHeight: 25, space: 4, allowBottomResize: false, allowLeftResize: false, allowRightResize: false, height: '100%', onHeightChanged: f_heightChanged, isRightCollapse: true });
            var height = $(".l-layout-center").height();

            accordion = $("#accordion1").ligerAccordion({ height: height - 34 });

            menu();
        });

        function f_heightChanged(options) {
            if (tab)
                tab.addHeight(options.diff);
            if (accordion && options.middleHeight - 34 > 0)
                accordion.setHeight(options.middleHeight - 34);

        }


        function f_user() {
            $("#userinfo").hover(
                function () {
                    $(this).addClass("userover");
                },
                function () {
                    $(this).removeClass("userover");
                }
            )

        }

        function menu() {
            var mainmenu = $("#accordion1");
            $.getJSON("Sys_base.GetAllMenus.xhd?rnd=" + Math.random(), function (data, textStatus) {
                $(data).each(function (i, app) {
                    var appmenu = $("<div title='" + app.text + "'><ul class='sidebar-menu'></ul></div>");

                    $(app.children).each(function (gi, group) //包括分组的部分
                    {
                        if (group.children) {
                            var groupmenu = $("<li class='sub-menu'><a ><img />" + group.Menu_name + "<span class='arrow'></span></a><ul class='sub'></ul><li>");
                            groupmenu.find("img").attr("src", group.Menu_icon);

                            $(group.children).each(function (i, submenu) {
                                var subitem = $('<li><a class="menulink" ><img />' + submenu.Menu_name + '</a></li>');
                                subitem.find("img").attr("src", submenu.Menu_icon);
                                subitem.find("a").attr({
                                    tabid: submenu.Menu_id,
                                    tabtext: submenu.Menu_name,
                                    taburl: submenu.Menu_url
                                });
                                $("ul:first", groupmenu).append(subitem);
                            });
                            $("ul:first", appmenu).append(groupmenu);
                        }
                        else {
                            var subitem = $('<li><a class="menulink" ><img />' + group.Menu_name + '</a></li>');
                            subitem.find("img").attr("src", group.Menu_icon);
                            subitem.find("a").attr({
                                tabid: group.Menu_id,
                                tabtext: group.Menu_name,
                                taburl: group.Menu_url
                            });
                            $("ul:first", appmenu).append(subitem);
                        }

                    });
                    $(mainmenu).append(appmenu);
                })

                accordion.render();
                onResize();

                $('.sub-menu > a').click(function () {
                    var last = $('.sub-menu.open');
                    last.removeClass("open");
                    $('.sub').slideUp(200);

                    var sub = jQuery(this).next();
                    if (sub.is(":visible")) {
                        $(this).parent().removeClass("open");
                        sub.slideUp(200);
                    } else {
                        $(this).parent().addClass("open");
                        sub.slideDown(200);
                    }
                });

                mainmenu.find("a.menulink").click(function () {
                    var tabid = $(this).attr('tabid'),
                       url = $(this).attr("taburl"),
                       text = $(this).attr('tabtext');

                    if (!url) return;

                    f_addTab(tabid, text, url);
                });

                //$(".l-accordion-header").css({"border-top":"1px solid #fff","line-height":"29px"})
                $("#home").attr("src", "home/home.aspx");
                $("#pageloading").fadeOut(800);
            })
        }



        function f_dropdown(e) {
            var sysitem = [];
            var windowsswitch;
            if ($(".l-userinfo-panel").length == 0) {
                windowsswitch = $("<div class='l-userinfo-panel'><ul class='userinfolist'></ul></div>").appendTo($("#userinfo"));
                sysitem.push({ icon: 'images/icon/1.png', title: "退出系统", click: function () { logout(); } });

                $(sysitem).each(function (i, item) {
                    var subitem = $('<li><img/><span></span></li>');

                    $("img", subitem).attr("src", item.icon);
                    $("span", subitem).html(item.title);
                    $("ul:first", windowsswitch).append(subitem);
                    subitem.click(function () { item.click(item); });
                })
            }
            else
                windowsswitch = $(".l-userinfo-panel");

            $("li", windowsswitch).live('click', function () {
                $(".l-userinfo-panel").hide();
            }).live('mouseover', function () {
                var jitem = $(this);
                jitem.addClass("over");
            }).live('mouseout', function () {
                var jitem = $(this);
                jitem.removeClass("over");
            });
            windowsswitch.css({
                top: $("#userinfo").offset().top + $("#userinfo").height() + 10,
                width: $("#userinfo").width()
            });

            if ($(".l-userinfo-panel").css('display') == 'none')
                $(".l-userinfo-panel").show();
            else
                $(".l-userinfo-panel").hide();

            $(document).one("click", function () {
                $(".l-userinfo-panel").hide();
            });

            e.stopPropagation();
        }

        function getuserinfo() {
            $.getJSON("Sys_base.GetUserInfo.xhd?rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                $("#Username").html("<div style='cursor:pointer'>" + data.name + "</div>");
                if (data.title)
                    $("#userheader").attr("src", data.title);
                else
                    $("#userheader").attr("src", "/images/noheadimage.jpg");
            });
        }



        function getUser() {
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'Sys_base.getUserTree.xhd',
                data: { rnd: Math.random() },
                success: function (result) {

                },
                error: function () {
                    javascript: location.replace("login.aspx");
                }

            });
        }
        function logout() {
            $.ligerDialog.confirm('您确认要退出系统？', function (yes) {
                if (yes) {
                    $.ajax({
                        type: 'post',
                        //dataType: 'json',
                        url: 'login.logout.xhd',
                        //data: { type: "login", method: 'logout' },
                        success: function (result) {
                            javascript: location.replace("login.aspx");
                        },
                        error: function ()
                        { alert() }

                    });
                }
            });
        }


    </script>

</body>
</html>
