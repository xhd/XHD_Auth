<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/touch/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/input.css" rel="stylesheet" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            $("form").ligerForm();

            loadForm(getparastr("cid"));

            toolbar();
            
        })
        function toolbar() {
            $.getJSON("toolbar.GetSys.xhd?mid=contact_follow&rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu1 = $.ligerMenu({
                    width: 120,
                    items: getMenuItems(data)
                });
            });
        }

        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&Action=save&id=" + getparastr("cid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
        var a; var b; var c; var d; var e; var f; var g; var h; var i;

        function loadForm(oaid) {
            //var dialog = frameElement.dialog;
            //dialog.setShowToggle(0);
            $.ajax({
                type: "GET",
                url: "CRM_Customer.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    //alert(obj.constructor); //String 构造函数

                    $("#form1").ligerAutoForm({
                        labelWidth: 80, inputWidth: 180, labelWidth: 80, space: 20,
                        fields: [
                            {
                                display: '客户', type: 'group', icon: '',
                                rows: [
                                    [
                                        { display: "客户名称", name: "T_customer", type: "text", options: "{width:180}", validate: "{required:true}", initValue: obj.cus_name },
                                        { display: "电话", name: "T_tel", type: "text", options: "{width:180}", validate: "{required:true}", initValue: obj.cus_tel }
                                    ],                                   
                                    [
                                        { display: "备注", name: "T_remarks", type: "text", options: "{width:465}", width: 469, initValue: obj.Remarks },
                                    ],
                                    [
                                       
                                        { display: "业务员", name: "T_employee", validate: "{required:true}" }
                                    ]
                                ]
                            }
                        ]
                    });
                   
                    $('#T_employee').ligerComboBox({ width: 180, onBeforeOpen: f_selectContact });
                    $("#T_customer").attr("validate", "{ required: true, remote: remote, messages: { required: '请输入客户名', remote: '此客户已存在!' } }");

                    if (obj.emp_id) {
                        $("#T_employee_val").val(obj.emp_id);
                        $("#T_employee").val("【" + obj.department + "】" + obj.employee);
                    }
                    else {
                        $.getJSON("hr_employee.form.xhd?id=epu&rnd=" + Math.random(), function (result) {
                            var obj = eval(result);
                            for (var n in obj) {
                                if (obj[n] == null)
                                    obj[n] = "";
                            }
                            $("#T_employee_val").val(obj.id);
                            $("#T_employee").val("【" + obj.dep_name + "】" + obj.name);
                        })
                    }      
                }
            });
        }
        function f_selectContact() {
            top.$.ligerDialog.open({
                zindex: 9003,
                title: '选择员工', width: 850, height: 400, url: "hr/Getemp_Auth.aspx?auth=1", buttons: [
                    { text: '确定', onclick: f_selectContactOK },
                    { text: '取消', onclick: f_selectContactCancel }
                ]
            });
            return false;
        }
        function f_selectContactOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择员工!');
                return;
            }

            $("#T_employee").val("【" + data.dep_name + "】" + data.name);
            $("#T_employee_val").val(data.id);

            dialog.close();
        }

        function f_selectContactCancel(item, dialog) {
            dialog.close();
        }
        function remote() {
            var url = "CRM_Customer.validate.xhd?type=cus&T_cid=" + getparastr("cid") + "&rnd=" + Math.random();
            return url;
        }
       

    </script>
</head>
<body>
    <div style="padding: 10px">
        <form id="form1" onsubmit="return false">
            <input type="hidden" id="T_xy" name="T_xy" />
        </form>

    </div>
</body>
</html>
